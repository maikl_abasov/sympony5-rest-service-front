Vue.component('items-header', {
    props : ['title', 'num', 'subtitle'],
    data: function () {
        return {

        }
    },
    template: `
        <div class="shadow-lg">
            <div class="pt-3 pb-4 ">
              <a href="#" class="block bg-white py-3 border-t">
                <div class="px-4 py-2 flex  justify-between">
                  <span>{{title}}</span>
                  <div>
                    <span class="px-3 text-sm font-semibold">{{num}}</span>
                    <span class="text-sm font-semibold px-4 py-1 text-gray-800 rounded-full bg-green-300">{{subtitle}}</span>
                  </div>
                </div>
              </a>
            </div>
        </div>
    `,
})
