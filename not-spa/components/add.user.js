Vue.component('add-user', {
    props : ['role', 'room_id'],
    data: function () {
        return {
            newUser : {
                fio : '',
                email : '',
                phone : '',
                password : '',
                role  : 3,
                car_showroom_id : 0,
            },
        }
    },

    created() {
        this.newUser['role'] = this.role;
        this.newUser['car_showroom_id'] = this.room_id;
    },

    methods: {
        save() {
            this._post('/user/create', this.newUser).then(resp => {
                if(resp.user_id) {
                    alert('Пользователь успешно создан');
                    return true;
                }
                alert('Не удалось создать пользователя');
            })
        },
    },

    template: `
    <div class="add-user-container">
            <!-- component -->

            <div class="min-h-screen bg-gray-100 p-0 sm:p-12">
              <div class="mx-auto max-w-md px-6 py-12 bg-white border-0 shadow-lg sm:rounded-3xl">
                <h1 class="text-2xl font-bold mb-8" style="text-align: center">Регистрация пользователя</h1>
                <div id="form" >
                
                  <!--- FIO --->
                  <div class="relative z-0 w-full mb-5 form-box">
                        <label for="name" class="absolute duration-300 top-3 -z-1 origin-0 text-gray-500">ФИО</label><br/>
                        <input class="pt-3 pb-2 block w-full px-0 mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 focus:border-black border-gray-200"
                               v-model="newUser.fio" type="text" placeholder=""  required />
                        <span class="text-sm text-red-600 hidden" id="error">Name is required</span>
                  </div>
 
                  <!--- Email --->
                  <div class="relative z-0 w-full mb-5 form-box">
                        <label for="name" class="absolute duration-300 top-3 -z-1 origin-0 text-gray-500">Email</label><br/>
                        <input class="pt-3 pb-2 block w-full px-0 mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 focus:border-black border-gray-200"
                               v-model="newUser.email" type="text" placeholder=""  required />
                        <span class="text-sm text-red-600 hidden" id="error">Email is required</span>
                  </div>
                  
                  <!--- Password --->
                  <div class="relative z-0 w-full mb-5 form-box">
                        <label for="name" class="absolute duration-300 top-3 -z-1 origin-0 text-gray-500">Пароль</label><br/>
                        <input class="pt-3 pb-2 block w-full px-0 mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 focus:border-black border-gray-200"
                               v-model="newUser.password" type="text" placeholder=""  required />
                        <span class="text-sm text-red-600 hidden" id="error">Password is required</span>
                  </div>
                  
                  
                  <!--- Phone --->
                  <div class="relative z-0 w-full mb-5 form-box "> 
                        <label for="name" class="absolute duration-300 top-3 -z-1 origin-0 text-gray-500">Телефон</label><br/>
                        <input class="pt-3 pb-2 block w-full px-0 mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 focus:border-black border-gray-200"
                               v-model="newUser.phone" type="text"   placeholder=""  required />
                        <span class="text-sm text-red-600 hidden" id="error">Phone is required</span>
                  </div> 
                  
                  <!--- Role --->
                  <div class="relative z-0 w-full mb-5 form-box">
                        <label for="name" class="absolute duration-300 top-3 -z-1 origin-0 text-gray-500">Роль</label> <br/>
                        <input class="pt-3 pb-2 block w-full px-0 mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 focus:border-black border-gray-200"
                               v-model="newUser.role" type="text"  placeholder=""  required />
                        <span class="text-sm text-red-600 hidden" id="error">Role is required</span>
                  </div> 
                  
                  <!--- ShowRoomId --->
                  <div class="relative z-0 w-full mb-5 form-box">
                        <label for="name" class="absolute duration-300 top-3 -z-1 origin-0 text-gray-500">Салон</label><br/>
                        <input class="pt-3 pb-2 block w-full px-0 mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 focus:border-black border-gray-200"
                               v-model="newUser.car_showroom_id" type="text"  placeholder=""   /> 
                  </div> 

                  <!--- Кнопка сохранения--->
                  <button class="w-full px-6 py-3 mt-3 text-lg text-white transition-all duration-150 ease-linear rounded-lg shadow outline-none bg-green-500 hover:bg-pink-600 hover:shadow-lg focus:outline-none"
                          @click="save()" id="button"  type="button" >
                          Сохранить
                  </button>
                  
                </div>
              </div>
            </div>


    </div>`,
})
