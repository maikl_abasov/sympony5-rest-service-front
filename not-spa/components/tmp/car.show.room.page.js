
Vue.component('car-show-room-page', {
    data: function () {
        return {
            currentRoomId : 0,
            roomTitle     : '',
        }
    },

    methods: {

        getShowRoomInfo(item) {
            let roomId = item.id
            this.currentRoomId = item.id
            this.roomTitle = 'Автосалон-' + roomId
            this.getRoomCars (roomId)
            this.getRoomManagers(roomId)
        },

        getRoomManagers(roomId) {

        },

    },

    template: `
      <div>
      
        <div class="h-screen flex flex-col">

    <div class="flex-1 flex overflow-x-hidden">

      <!--- Левое меню --->
      <div class="w-64 p-6 bg-gray-100 overflow-y-auto">
        <nav>
            <h2 class="font-semibold text-gray-600 uppercase tracking-wide">Автосалоны</h2>
            <div v-for="(item) in showRooms.list" class="mt-3" :key="item.id" style="border-bottom:2px gainsboro solid">
                <a  @click="getShowRoomInfo(item)" class="-mx-3  py-1 px-3 text-sm font-medium flex items-center justify-between hover:bg-gray-200 rounded-lg"  style="cursor: pointer">
                      <div style="width: 140px;border: 0px grey solid; text-align: left"> <i class="h-6 w-6 fa fa-user-o fill-current text-gray-700" aria-hidden="true"></i>
                             <span class=" text-gray-900">Автосалон-{{item.id}}</span>
                      </div><br>
                      <!-- <span class="inline-block px-4 py-1 text-center py-1 leading-none text-xs font-semibold text-gray-700 bg-gray-300 rounded-full">{{item.id}}</span>-->
                      <div style="font-size: 11px; font-style: italic; margin-left: 10px; border-left: 1px grey solid; width: 100px; padding-left: 5px;">Адрес:{{item.address}}</div>
                </a>
            </div>
        </nav>
      </div>

      <main class=" flex bg-gray-200">

        <!--- 1 block -->
        <div class="overflow-y-auto overflow-hidden">
          <div class="px-4 py-2 flex items-center justify-between border-l border-r border-b">
              <button class="text-sm flex items-center font-semibold text-gray-600">
                <span>Автомобили ({{roomTitle}})</span>
                <i class="ml-2 fa fa-angle-down justify-between" aria-hidden="true"></i>
              </button>
              <button class="text-sm flex items-center font-semibold text-gray-600">
                <i class="fa fa-bars" aria-hidden="true"></i>
              </button>
          </div>
          <div class="pt-3 pb-4 ">
              <div v-for="(item) in cars.cars" class="block bg-white py-3 border-t" :key="item.id">
                <div class="text-sm font-semibold text-gray-900 px-4 py-2" style="text-align: left">
                    Марка : {{item.brand}}
                </div>
                <div class="px-4 py-2 flex  justify-between">
                    <span class="text-sm font-semibold text-gray-900" >Модель : {{item.model}}</span>
                    <span class="text-sm font-semibold text-gray-600" style="margin-left:40px;">{{item.price}}</span>
                </div>
              </div>
          </div>
        </div>

        <!--- 2 block -->
        <div class="flex flex-col py-3 w-auto inline-block overflow-y-auto overflow-hidden bg-gray-100">

          <div class="shadow-lg">
            <div class="pt-3 pb-4 ">
              <a href="#" class="block bg-white py-3 border-t">
                <div class="px-4 py-2 flex  justify-between">
                  <span>Менеджеры</span>
                  <div>
                    <span class="px-3 text-sm font-semibold">#1428</span>
                    <span class="text-sm font-semibold px-4 py-1 text-gray-800 rounded-full bg-green-300">Active</span>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div>
             <div v-for="(item) in managers.users" class="shadow-lg pt-4 ml-2 mr-2 rounded-lg" :key="item.id">
                <a class="block bg-white py-3 border-t pb-4">
                  <div class="px-4 py-2 flex  justify-between">
                    <span class="text-sm font-semibold text-gray-900">{{item.fio}}</span>
                    <div class="flex">
                      <span class="px-4 text-sm font-semibold text-gray-600"> {{item.phone}}</span>
                      <img class="h-6 w-6 rounded-full object-cover"
                           src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=144&q=80">
                    </div>
                  </div>

                  <div class="px-4 py-2 flex  justify-between">
                      <span class="text-sm font-semibold text-gray-900">Email : {{item.email}}</span>
                      <div class="flex">
                          <span class="px-4 text-sm font-semibold text-gray-600">Роль : {{item.role}}</span>
                      </div>
                  </div>
                </a>
             </div>
          </div>



        </div>

        <!--- 3 block -->
        <div class="flex flex-col py-3 w-auto inline-block overflow-y-auto overflow-hidden bg-gray-100">

          <div class="shadow-lg">
            <div class="pt-3 pb-4 ">
              <a href="#" class="block bg-white py-3 border-t">
                <div class="px-4 py-2 flex  justify-between">
                  <span>Продажи</span>
                  <div>
                    <span class="px-3 text-sm font-semibold">#1428</span>
                    <span class="text-sm font-semibold px-4 py-1 text-gray-800 rounded-full bg-green-300">Количество:</span>
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div>

            <div v-for="(item) in orders.orders" class="shadow-lg pt-4 ml-2 mr-2 rounded-lg" :key="item.id">
              <a class="block bg-white py-3 border-t pb-4">

                <div class="px-4 py-2 flex  justify-between">
                  <span class="text-sm font-semibold text-gray-900">Покупатель : {{item.client_id}}</span>
                </div>

                <div class="px-4 py-2 flex  justify-between">
                  <span class="text-sm font-semibold text-gray-900">Дата : {{item.create_dt}}</span>
                </div>

                <div class="px-4 py-2 flex  justify-between">
                  <span class="text-sm font-semibold text-gray-900">Товар : {{item.car_id}}</span>
                </div>

                <div class="px-4 py-2 flex  justify-between">
                     <span class="text-sm font-semibold text-gray-900">Менеджер : {{item.manager_id}}</span>
                </div>

              </a>
            </div>
          </div>

        </div>

      </main>
    </div>
  </div>
      
      </div>`,
})
