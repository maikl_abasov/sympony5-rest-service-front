<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Symfony5-front</title>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
            rel="stylesheet">
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <link rel="icon" href="style/style.css">
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="http://shop1.bolderp5.bget.ru/ARCHIVES/lg.js"></script>

    <script></script>
    <style>
        .car-show-room:hover {
            background: #7DA0B1;
        }
        .client-card:hover {
            background: #7DA0B1;
        }
    </style>
  </head>
  <body>
    <div id="app">

        <header class="flex flex-shrink-0">
            <div class="flex-shrink-0 px-4 py-3 bg-gray-800 w-64">
                <div class="flex items-center block w-full">
                    <template v-if="userFio">
                        <div class="ml-4 text-sm font-medium text-white">{{userFio}}</div><br/>
                        <div class="ml-4 text-sm font-medium text-white">({{userRole}})</div>
                    </template>
                    <template v-else >
                        <span class="ml-4 text-sm font-medium text-white">Not user</span>
                    </template>
                </div>
            </div>
            <div class="flex-1 flex bg-gray-700 px-6 items-center justify-between">

                <nav style="cursor:pointer">
                    <a @click="pageSwitcher('showrooms')" class="hover:bg-gray-600   bg-gray-800 inline-block text-sm font-medium text-white px-3 py-2 leading-none" >Автосалоны</a>
                    <a @click="pageSwitcher('users')" class="ml-2 hover:bg-gray-600  inline-block text-sm font-medium text-white px-3 py-2 leading-non" >Пользователи</a>
                    <a @click="pageSwitcher('add-objects')"  class="ml-2 hover:bg-gray-600  inline-block text-sm font-medium text-white px-3 py-2 leading-non" >Добавление объектов</a>
                </nav>

                <div class="flex items-center">
                    <span class="relative">
                        <span class="absolute inset-y-0 left-0 flex items-center">
                            <i class="fa fa-search h-5 w-5 px-2" style="color: gray;" aria-hidden="true"></i>
                        </span>
                        <input class="focus:bg-white focus:text-gray-900 focus:placeholder-gray-700 pl-10 pr-4 py-2 leading-none block w-full bg-gray-900 rounded-lg text-sm placeholder-gray-400 text-white"
                                placeholder="Search">
                    </span>
                    <button @click="logout()" class="ml-6 text-gray-400 hover:text-gray-200">
                        <i class="fa fa-bell-o h-5 w-5 fill-current" aria-hidden="true" style="color: #fff;">Выход</i>
                    </button>
                    <button class="ml-6 text-gray-400 hover:text-gray-200">
                        <i class="fa fa-question-circle-o h-5 w-5 fill-current" aria-hidden="true"
                           style="color: #fff;"></i>
                    </button>

                </div>
            </div>
        </header>

        <div class="main-content" >
                 <template v-if="pageName == 'showrooms'" >
                     <car-show-room-page></car-show-room-page>
                 </template>
                <template v-if="pageName == 'home'" >
                    Home
                </template>
                 <template v-else-if="pageName == 'users'" >
                    <users-page></users-page>
                 </template>
                 <template v-else-if="pageName == 'add-objects'" >
                     <add-objects-page></add-objects-page>
                 </template>
                <template v-else >
                    <auth-page v-if="!userId" ></auth-page>
                </template>
        </div>

    </div>
  </body>

  <script src="app/app.js" ></script>
  <script src="app/api.service.js" ></script>
  <script src="app/plugins.js" ></script>

  <script src="components/items.header.js" ></script>
  <script src="components/add.user.js" ></script>

  <script src="pages/car.show.room.js" ></script>
  <script src="pages/users.js" ></script>
  <script src="pages/auth.js" ></script>
  <script src="pages/add.objects.js" ></script>

  <script >

      Vue.use(BaseMixin)
      Vue.use(ApiService)

      const app = new Vue({
          el: '#app',
          data: {
              userName : 'Not user',
              pageName : '',
          },

          created() {

               this.getUserInfo();
               let authStatus = this.checkUserAuth();
               let pageName   = 'showrooms';
               if(!authStatus) pageName = ''
               this.pageSwitcher(pageName);

          },

          methods : {

          },
      })
  </script>

</html>
