const BaseMixin = {

    install(Vue, options) {

        Vue.mixin({

            data: () => ({
                users: [],
                user: {},
                clients: [],
                managers: [],
                cars: [],
                car: {},
                orders: [],
                order: {},
                showRooms: [],
                showRoom: {},
                clientOrders: [],

                currentRoomId: 0,
                roomTitle: '',
                carId    : 0,
                orderId  : 0,
                clientId : 0,

                userId   : 0,
                userRole : '',
                userFio  : '',
            }),

            created() {
                this.getUsers()
                this.getClients()
                this.getManagers()
                this.getOrders()
                this.getCars()
                this.getShowRooms()
            },

            methods: {

                getUsers() {
                    this._get('/user/list/all').then(resp => {
                        this.users = resp
                    })
                },

                getUser(userId) {
                    this._get('/user/one/' + userId).then(resp => {
                        this.user = resp
                    })
                },

                getClients() {
                    this._get('/user/list/clients').then(resp => {
                        this.clients = resp
                    })
                },

                getManagers() {
                    this._get('/user/list/managers').then(resp => {
                        this.managers = resp
                    })
                },

                getCars() {
                    this._get('/car/list/all').then(resp => {
                        this.cars = resp
                    })
                },

                getCar(carId) {
                    this._get('/car/one/' + carId).then(resp => {
                        this.car = resp
                    })
                },

                getRoomCars(showRoomId) {
                    this._get('/car/list/showroom/' + showRoomId).then(resp => {
                        this.cars = resp
                    })
                },

                getOrders() {
                    this._get('/order/list/all').then(resp => {
                        this.orders = resp
                    })
                },

                getOrder(orderId) {
                    this._get('/order/one/' + orderId).then(resp => {
                        this.order = resp
                    })
                },

                getClientOrders(item) {
                    let clientId = item.id
                    this.clientId = clientId;
                    this._get('/order/list/by_client/' + clientId).then(resp => {
                        this.clientOrders = resp
                    })
                },

                getShowRooms() {
                    this._get('/showroom/list/all').then(resp => {
                        this.showRooms = resp
                    })
                },

                getShowRoom(roomId) {
                    this._get('/showroom/one/' + roomId).then(resp => {
                        this.showRoom = resp
                    })
                },

                getShowRoomInfo(item) {
                    let roomId = item.id
                    this.currentRoomId = item.id
                    this.roomTitle = 'Автосалон-' + roomId
                    this.getRoomCars(roomId)
                    this.getRoomManagers(roomId)
                    this.getRoomOrders(roomId)
                },

                getRoomManagers(roomId) {
                    this._get('/showroom/managers/' + roomId).then(resp => {
                        this.managers = resp
                    })
                },

                getRoomOrders(roomId) {
                    this._get('/showroom/orders/' + roomId).then(resp => {
                        this.orders = resp
                    })
                },

                usersLoad(role) {
                    this.userRole = role;
                    switch (type) {
                        case 2 : this.getManagers();
                                 break;
                        case 3 : this.getClients();
                                 break;
                    }
                },

                getLocalStore(key) {
                    let value = localStorage.getItem(key);
                    return (value) ? value : false
                },

                setLocalStore(key, value) {
                    localStorage.setItem(key, value);
                },

                deleteLocalStore(key) {
                    localStorage.removeItem(key);
                },

                setUserInfo(user, token) {
                    this.setLocalStore('token', token);
                    this.setLocalStore('user_id', user.id);
                    this.setLocalStore('role', user.role);
                    this.setLocalStore('fio', user.fio);
                },

                deleteUserInfo() {
                    this.deleteLocalStore('token');
                    this.deleteLocalStore('user_id');
                    this.deleteLocalStore('role');
                    this.deleteLocalStore('fio');
                    this.userRole = ''
                    this.userFio  = ''
                    this.userId   = 0
                },

                getUserRole() {
                    let role = parseInt(this.getLocalStore('role'));
                    switch (role) {
                        case 2  : this.userRole = 'Менеджер'; break;
                        case 3  : this.userRole = 'Клиент';   break;
                        default : this.userRole = ''; break;
                    }
                },

                getUserFio() {
                    let fio = this.getLocalStore('fio');
                    if(fio) this.userFio = fio;
                },

                getUserId() {
                    let id = this.getLocalStore('user_id');
                    if(id) this.userId = id;
                },

                checkUserAuth() {
                    if(this.userId && this.userFio && this.userRole) return true;
                    return false;
                },

                pageSwitcher(name) {
                    let authStatus = this.checkUserAuth();
                    let pageName   = name;
                    if(!authStatus) {
                        pageName = '';
                        // this.redirectUrl(location.href)
                    }
                    this.pageName = pageName
                },

                getUserInfo() {
                    this.getUserRole();
                    this.getUserFio();
                    this.getUserRole();
                    this.getUserId();
                },

                redirectUrl(href) {
                    window.location.href = href;
                },

                logout() {
                    this.deleteUserInfo();
                    this.redirectUrl(location.href)
                },

            } // Methods
        }) // VueMixin
    }
}

