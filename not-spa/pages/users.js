Vue.component('users-page', {
    data: function () {
        return {
           userRole : 1,
        }
    },

    methods: {

    },

    template: `
    <div class="h-screen flex flex-col">

            <div class="flex-1 flex overflow-x-hidden">
        
              <div class="overflow-y-auto overflow-hidden" 
                  style="border: 1px gainsboro solid; width: 255px; margin-right: 0px; background: aliceblue;">
              
                  <div class="px-4 py-2 flex items-center justify-between border-l border-r border-b" style="text-align: center">
                      <button class="text-sm flex items-center font-semibold text-gray-600" style="text-align: center">
                          <div style="text-align: center" >Пользователи</div>
                      </button>
                  </div>
                  
                  <div class="pt-3 pb-4 " style="padding: 5px;">

                       <div  class="mt-3" class="block bg-white py-3 border-t car-show-room" style="cursor: pointer" 
                           @click="usersLoad(2)" >
                           <div class="text-sm font-semibold text-gray-900 px-4 py-2" style="text-align: left">
                                Менеджеры
                           </div>
                       </div>
                       
                        <div  class="mt-3" class="block bg-white py-3 border-t car-show-room" style="cursor: pointer" 
                           @click="usersLoad(3)" >
                           <div class="text-sm font-semibold text-gray-900 px-4 py-2" style="text-align: left">
                                Клиенты
                           </div>
                       </div>
                       
                  </div>
              </div>

              <main class=" flex bg-gray-200">

                    <!--- 2 block(менеджеры) -->
                    <div class="flex flex-col py-3 w-auto inline-block overflow-y-auto overflow-hidden bg-gray-100">
                    
                      <template v-if="userRole == 2">
                          <items-header title="Менеджеры" num="#1428" subtitle="Active"></items-header>
                          <div>
                             <div v-for="(item) in managers.users" class="shadow-lg pt-4 ml-2 mr-2 rounded-lg" :key="item.id">
                                <a class="block bg-white py-3 border-t pb-4">
                                      <div class="px-4 py-2 flex  justify-between">
                                        <span class="text-sm font-semibold text-gray-900">{{item.fio}}</span>
                                        <div class="flex">
                                          <span class="px-4 text-sm font-semibold text-gray-600"> {{item.phone}}</span>
                                          <img class="h-6 w-6 rounded-full object-cover"
                                               src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=144&q=80">
                                        </div>
                                      </div>
                    
                                      <div class="px-4 py-2 flex  justify-between">
                                          <span class="text-sm font-semibold text-gray-900">Email : {{item.email}}</span>
                                          <div class="flex">
                                              <span class="px-4 text-sm font-semibold text-gray-600">Роль : {{item.role}}</span>
                                          </div>
                                      </div>
                                </a>
                             </div>
                          </div>
                          
                      </template>
                      <template v-else-if="userRole == 3">
                           <items-header title="Клиенты" num="#1428" subtitle="Active"></items-header>
                           <div>
                             <div v-for="(item) in clients.users" class="shadow-lg pt-4 ml-2 mr-2 rounded-lg" :key="item.id">
                                <a @click="getClientOrders(item)" :key="item.id" class="block bg-white py-3 border-t pb-4 client-card" style="cursor: pointer">
                                      <div class="px-4 py-2 flex  justify-between">
                                        <span class="text-sm font-semibold text-gray-900">{{item.fio}}</span>
                                        <div class="flex">
                                          <span class="px-4 text-sm font-semibold text-gray-600"> {{item.phone}}</span>
                                          <img class="h-6 w-6 rounded-full object-cover"
                                               src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=144&q=80">
                                        </div>
                                      </div>
                    
                                      <div class="px-4 py-2 flex  justify-between">
                                          <span class="text-sm font-semibold text-gray-900">Email : {{item.email}}</span>
                                          <div class="flex">
                                              <span class="px-4 text-sm font-semibold text-gray-600">Роль : {{item.role}}</span>
                                          </div>
                                      </div>
                                </a>
                             </div>
                          </div> 
                      </template>
            
                    </div>
            
                    <!--- 3 block(продажи) -->
                    <div class="flex flex-col py-3 w-auto inline-block overflow-y-auto overflow-hidden bg-gray-100">
  
                      <template v-if="userRole == 3 && clientId">
                          <items-header title="Покупки клиента" num="#1428" subtitle="Active"></items-header>
                          <div>
                            <div v-for="(item) in clientOrders.orders" class="shadow-lg pt-4 ml-2 mr-2 rounded-lg" :key="item.id">
                              <a class="block bg-white py-3 border-t pb-4">
                                    <div class="px-4 py-2 flex  justify-between">
                                        <span class="text-sm font-semibold text-gray-900">Покупатель : {{item.client_id}}</span>
                                    </div>
                                    <div class="px-4 py-2 flex  justify-between">
                                        <span class="text-sm font-semibold text-gray-900">Дата : {{item.create_dt}}</span>
                                    </div>      
                                    <div class="px-4 py-2 flex  justify-between">
                                        <span class="text-sm font-semibold text-gray-900">Товар : {{item.car_id}}</span>
                                    </div>
                                    <div class="px-4 py-2 flex  justify-between">
                                       <span class="text-sm font-semibold text-gray-900">Менеджер : {{item.manager_id}}</span>
                                    </div>
                              </a>
                            </div>
                          </div>
                      </template>
            
                    </div>
        
              </main>
              
            </div>
          
      </div>`,
})
