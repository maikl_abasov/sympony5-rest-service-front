Vue.component('auth-page', {
    data: function () {
        return {
            userRole: 0,
            userId: 0,
            userName: '',
            email : '',
            password : '',
        }
    },

    methods: {
        auth(email, password) {
            this._post('/user/jwt/auth', { email, password }).then(resp => {
                if(resp.token && resp.status) {
                    this.setUserInfo(resp.user, resp.token);
                    alert('Авторизация прошла успешно');
                    this.redirectUrl(location.href)
                    return true;
                }
                alert('Не удалось авторизировать пользователя,попробуйте еще раз');
            })
        },

        forgotPassword(email) {

        }
    },

    template: `
    <div class="auth-container">
     
         <!-- component -->
        <div class="container max-w-md mx-auto xl:max-w-3xl h-full flex bg-white rounded-lg shadow overflow-hidden" 
             style="margin-top: 20px">
             
              <div class="relative hidden xl:block xl:w-1/2 h-full" >
                    <img class="absolute h-auto w-full object-cover"
                         src="https://images.unsplash.com/photo-1541233349642-6e425fe6190e"
                         alt="my zomato"  style="border-radius: 0px"/>
              </div>
              <div class="w-full xl:w-1/2 p-8">
                <form method="post" action="#" onSubmit="return false">
                  <h1 class=" text-2xl font-bold">Войдите в свой аккаунт</h1>
                  <div>   
                    <span class="text-gray-600 text-sm">  Нет учетной записи?</span>
                    <span class="text-gray-700 text-sm font-semibold">Зарегистрироваться</span>
                  </div>
                  <div class="mb-4 mt-6">
                    <!--- EMAIL --->
                    <label  class="block text-gray-700 text-sm font-semibold mb-2"   htmlFor="email" >
                       Email
                    </label>
                    <input
                      class="text-sm appearance-none rounded w-full py-2 px-3 text-gray-700 bg-gray-200 leading-tight focus:outline-none focus:shadow-outline h-10"
                      id="email"  type="text"
                      placeholder="Your email address" 
                      v-model="email"/>
                  </div>
                  <div class="mb-6 mt-6">
                    <!--- PASSWORD --->
                    <label   class="block text-gray-700 text-sm font-semibold mb-2"  htmlFor="password"  >
                       Пароль
                    </label>
                    <input
                      class="text-sm bg-gray-200 appearance-none rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight focus:outline-none focus:shadow-outline h-10"
                      id="password"    type="password"
                      placeholder="Your password"
                       v-model="password" />
                       
                    <a class="inline-block align-baseline text-sm text-gray-600 hover:text-gray-800"
                       @click="forgotPassword(email)" > Забыли пароль? </a>
                       
                  </div>
                  <div class="flex w-full mt-8">
                    <!--- SUBMIT --->
                    <button @click="auth(email, password)" type="button"     
                        class="w-full bg-gray-800 hover:bg-grey-900 text-white text-sm py-2 px-4 font-semibold rounded focus:outline-none focus:shadow-outline h-10" > 
                        Войти 
                    </button>
                  </div>
                </form>
              </div>
          
        </div>
    </div>`,
})
