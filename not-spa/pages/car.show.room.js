Vue.component('car-show-room-page', {
    data: function () {
        return {

        }
    },

    methods: {

    },

    template: `
    <div class="h-screen flex flex-col">

            <div class="flex-1 flex overflow-x-hidden">
        
              <div class="overflow-y-auto overflow-hidden" 
                  style="border: 1px gainsboro solid; width: 255px; margin-right: 0px; background: aliceblue;">
              
                  <div class="px-4 py-2 flex items-center justify-between border-l border-r border-b" style="text-align: center">
                      <button class="text-sm flex items-center font-semibold text-gray-600" style="text-align: center">
                          <div style="text-align: center" >Автосалоны</div>
                          <i v-if="roomTitle" class="ml-2 fa fa-angle-down justify-between" aria-hidden="true"> | {{roomTitle}}</i>
                      </button>
                      <button class="text-sm flex items-center font-semibold text-gray-600">
                          <i class="fa fa-bars" aria-hidden="true"></i>
                      </button>
                  </div>
                  
                  <div class="pt-3 pb-4 " style="padding: 5px;">
                      <div v-for="(item) in showRooms.list" class="mt-3" :key="item.id"class="block bg-white py-3 border-t car-show-room" style="cursor: pointer" 
                           @click="getShowRoomInfo(item)" >
                           <div class="text-sm font-semibold text-gray-900 px-4 py-2" style="text-align: left">
                                Автосалон - {{item.id}}
                          </div>
                          <div class="px-4 py-2 flex  justify-between">
                                <span class="text-sm font-semibold text-gray-900" >Адрес:{{item.address}}</span>
                         </div>  
                      </div>
                      
                       <div  class="mt-3" class="block bg-white py-3 border-t car-show-room" style="cursor: pointer" 
                           @click="getCars()" >
                           <div class="text-sm font-semibold text-gray-900 px-4 py-2" style="text-align: left">
                                Показать все авто
                           </div>
                       </div>
                  </div>
              </div>

              <main class=" flex bg-gray-200">
 
                    <!--- 1 block(автомобили) -->
                    <div class="flex flex-col py-3 w-auto inline-block overflow-y-auto overflow-hidden bg-gray-100">
                
                      <items-header title="Автомобили" num="#1428" subtitle="Active"></items-header>
            
                      <div>
                         <div v-for="(item) in cars.cars"  :key="item.id" class="shadow-lg pt-4 ml-2 mr-2 rounded-lg" >
                            <a class="block bg-white py-3 border-t pb-4">
                                  <div class="px-4 py-2 flex  justify-between">
                                    <span class="text-sm font-semibold text-gray-900">Марка : {{item.brand}}</span>
                                  </div>
                                  <div class="px-4 py-2 flex  justify-between">
                                    <span class="text-sm font-semibold text-gray-900">Модель : {{item.model}}</span>
                                  </div>
                                  <div class="px-4 py-2 flex  justify-between">
                                    <span class="text-sm font-semibold text-gray-900">Цена : {{item.price}}</span>
                                  </div> 
                            </a>
                         </div>
                      </div>
            
                    </div>
            
                    <!--- 2 block(менеджеры) -->
                    <div class="flex flex-col py-3 w-auto inline-block overflow-y-auto overflow-hidden bg-gray-100">
                      <items-header title="Менеджеры" num="#1428" subtitle="Active"></items-header>
            
                      <div>
                         <div v-for="(item) in managers.users" class="shadow-lg pt-4 ml-2 mr-2 rounded-lg" :key="item.id">
                            <a class="block bg-white py-3 border-t pb-4">
                              <div class="px-4 py-2 flex  justify-between">
                                <span class="text-sm font-semibold text-gray-900">{{item.fio}}</span>
                                <div class="flex">
                                  <span class="px-4 text-sm font-semibold text-gray-600"> {{item.phone}}</span>
                                  <img class="h-6 w-6 rounded-full object-cover"
                                       src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=144&q=80">
                                </div>
                              </div>
            
                              <div class="px-4 py-2 flex  justify-between">
                                  <span class="text-sm font-semibold text-gray-900">Email : {{item.email}}</span>
                                  <div class="flex">
                                      <span class="px-4 text-sm font-semibold text-gray-600">Роль : {{item.role}}</span>
                                  </div>
                              </div>
                            </a>
                         </div>
                      </div>
            
                    </div>
            
                    <!--- 3 block(продажи) -->
                    <div class="flex flex-col py-3 w-auto inline-block overflow-y-auto overflow-hidden bg-gray-100">
  
                      <items-header title="Продажи" num="#1428" subtitle="Active"></items-header>
            
                      <div>
            
                        <div v-for="(item) in orders.orders" class="shadow-lg pt-4 ml-2 mr-2 rounded-lg" :key="item.id">
                          <a class="block bg-white py-3 border-t pb-4">
            
                            <div class="px-4 py-2 flex  justify-between">
                              <span class="text-sm font-semibold text-gray-900">Покупатель : {{item.client_id}}</span>
                            </div>
                            <div class="px-4 py-2 flex  justify-between">
                              <span class="text-sm font-semibold text-gray-900">Дата : {{item.create_dt}}</span>
                            </div>      
                            <div class="px-4 py-2 flex  justify-between">
                              <span class="text-sm font-semibold text-gray-900">Товар : {{item.car_id}}</span>
                            </div>
                            <div class="px-4 py-2 flex  justify-between">
                                 <span class="text-sm font-semibold text-gray-900">Менеджер : {{item.manager_id}}</span>
                            </div>
            
                          </a>
                        </div>
                      </div>
            
                    </div>
        
              </main>
              
            </div>
          
      </div>`,
})
