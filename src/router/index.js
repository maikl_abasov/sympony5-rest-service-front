import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },

  {
    path: '/showrooms',
    name: 'Showroom',
    component: () => import(/* webpackChunkName: "about" */ '../views/ShowRoom.vue')
  },

  {
    path: '/users',
    name: 'Users',
    component: () => import(/* webpackChunkName: "about" */ '../views/Users')
  },

  {
    path: '/add-objects',
    name: 'AddObjects',
    component: () => import(/* webpackChunkName: "about" */ '../views/AddObjects')
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
