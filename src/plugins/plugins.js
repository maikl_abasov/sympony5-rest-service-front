// import Vue from 'vue'
// import TopHeader from '../components/TopHeader';

const Plugin = {

  install (Vue, options) {
    // Vue.component('TopHeader'  , TopHeader)

    Vue.mixin({

      data: () => ({
        users: [],
        user: {},
        clients: [],
        managers: [],
        cars: [],
        car: {},
        orders: [],
        order: {},
        showRooms: [],
        showRoom: {},
        clientOrders: []
      }),

      created () {
        this.getUsers()
        this.getClients()
        this.getManagers()
        this.getOrders()
        this.getCars()
        this.getShowRooms ()
      },

      methods: {

        getUsers () {
          this._get('/user/list/all').then(resp => {
            this.users = resp
          })
        },

        getUser (userId) {
          this._get('/user/one/' + userId).then(resp => {
            this.user = resp
          })
        },

        getClients () {
          this._get('/user/list/clients').then(resp => {
            this.clients = resp
          })
        },

        getManagers () {
          this._get('/user/list/managers').then(resp => {
            this.managers = resp
          })
        },

        getCars () {
          this._get('/car/list/all').then(resp => {
            this.cars = resp
          })
        },

        getCar (carId) {
          this._get('/car/one/' + carId).then(resp => {
            this.car = resp
          })
        },

        getRoomCars (showRoomId) {
          this._get('/car/list/showroom/' + showRoomId).then(resp => {
            this.cars = resp
          })
        },

        getOrders () {
          this._get('/order/list/all').then(resp => {
            this.orders = resp
          })
        },

        getOrder (orderId) {
          this._get('/order/one/' + orderId).then(resp => {
            this.order = resp
          })
        },

        getClientOrders (clientId) {
          this._get('/order/list/by_client/' + clientId).then(resp => {
            this.clientOrders = resp
          })
        },

        getShowRooms () {
          this._get('/showroom/list/all').then(resp => {
             this.showRooms = resp
          })
        },

        getShowRoom (roomId) {
          this._get('/showroom/one/' + roomId).then(resp => {
            this.showRoom = resp
          })
        }

      } // Methods
    }) // VueMixin
  }
}

export default Plugin
