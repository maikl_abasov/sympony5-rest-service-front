import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Plugins from './plugins/plugins'
import ApiService from './plugins/api.service'

Vue.use(Plugins)
Vue.use(ApiService)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
